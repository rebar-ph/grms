<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

$env = 'default';

if(@$_SERVER['HTTP_HOST'] == 'localhost')
{
    $env = 'localhost';

}
else
{
    $env = 'devops';

}

//add base uri environment here
$baseuri = array();

$baseuri['localhost'] = 'http://localhost/GRMS/';
$baseuri['devops'] = 'http://72.55.179.189/GRMS/';

$database['localhost'] = array(
        'adapter'     => 'Mysql',
        'host'        => '127.0.0.1',
        'username'    => 'root',
        'password'    => '',
        'dbname'      => 'grms',
        'charset'     => 'utf8',
    );

$database['devops'] = array(
        'adapter'     => 'Mysql',
        'host'        => '72.55.179.189',
        'username'    => 'jpbernedo',
        'password'    => 'password',
        'dbname'      => 'grms',
        'charset'     => 'utf8',
        'port' => 'port'
);
//set constant for base uri and image uri
define("BASE_URI",$baseuri[$env]);

return new \Phalcon\Config(array(
    'database' => $database[$env],
    'application' => array(
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir'      => APP_PATH . '/app/models/',
        'migrationsDir'  => APP_PATH . '/app/migrations/',
        'viewsDir'       => APP_PATH . '/app/views/',
        'pluginsDir'     => APP_PATH . '/app/plugins/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'baseUri'        => $baseuri[$env],
    )
));


