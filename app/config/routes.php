<?php

use Phalcon\Mvc\Router as PhRouter;

$di->set('router', function(){
	$router = new PhRouter(false);
	
	$router->add('/', array(
		'controller'	=>	'index',
		'action'		=> 'index'
	)); 

	$router->add('/dashboard', array(
		'controller'	=>	'dashboard',
		'action'		=> 'index'
	)); 
	    
 	$router->add('/login', array(
		'controller'	=>	'index',
		'action'		=> 'login',
	));   
 	$router->add('/logout', array(
		'controller'	=>	'index',
		'action'		=> 'logout',
	));           
        
	return $router;

});
