<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
 	Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
	Phalcon\Db\RawValue as PhRawValue,
	Phalcon\Mvc\Url as PhUrl;

class BranchUser extends ModelBase
{
	public function initialize()
	{
		$this->setSource('common_branch_user_tb');	
	}
	
	public function getBranchUsers($branch_id)
	{
		$phql = "SELECT uit.user_id, uit.fname, uit.lname FROM BranchUser but
				 LEFT JOIN UserInfo uit ON uit.user_id = but.user_id
				 WHERE but.branch_id = '$branch_id'";
				 
		$data = $this->modelsManager->executeQuery($phql);
		return $data;
	}
	
}