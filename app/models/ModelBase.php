<?php

use Phalcon\Mvc\Url as PhUrl;

class ModelBase extends \Phalcon\Mvc\Model
{
    public $baseUri;

    protected function setBaseUri()
    {
		$url = new PhUrl();
        $this->baseUri = $url->getBaseUri();
    }

	protected function getBaseUri()
    {
        return $this->baseUri;
    }
}
