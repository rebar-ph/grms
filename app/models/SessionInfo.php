<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
 	Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
	Phalcon\Db\RawValue as PhRawValue,
	Phalcon\Mvc\Url as PhUrl;

class SessionInfo extends ModelBase
{
	public function initialize()
	{
		$this->setSource('common_session_tb');	
	}
}