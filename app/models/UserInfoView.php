<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
 	Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
	Phalcon\Db\RawValue as PhRawValue,
	Phalcon\Mvc\Url as PhUrl;

class UserInfoView extends ModelBase
{
    public function initialize() 
    {
        $this->setSource('user_info_vw');
    }

    public function getAllUserInfo($username, $password) 
    {
        $phql = "select * from user_info_vw where username='$username' and password='$password'";
        $wmsWorkflow = new WmsWorkflow();
        return new Resultset(null, $wmsWorkflow, $wmsWorkflow->getReadConnection()->query($phql));
    }

}